package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @Nullable
    Task create(@NotNull Task task);

    void updateById(@NotNull Task task);

    void removeByProjectId(@NotNull String userId, @NotNull String projectId);

}
