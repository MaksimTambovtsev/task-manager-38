package ru.tsc.tambovtsev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IDatabaseProperty;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    protected static final String TABLE_NAME = "TM_USER";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE " + LOGIN + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @NotNull final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE " + EMAIL + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, email);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @NotNull final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(ID));
        user.setLogin(row.getString(LOGIN));
        user.setPasswordHash(row.getString(PASSWORD_HASH));
        user.setEmail(row.getString(EMAIL));
        user.setFirstName(row.getString(FIRSTNAME));
        user.setLastName(row.getString(LASTNAME));
        user.setMiddleName(row.getString(MIDDLENAME));
        user.setRole(Role.valueOf(row.getString(ROLE)));
        user.setLocked(Boolean.valueOf(row.getString(LOCKED)));
        return user;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final User user) {
        @NotNull final String sql = "INSERT " + getTableName() +
                "(" +
                ID + ", " + LOGIN + ", " + PASSWORD_HASH + ", " + ROLE + ", " +
                LOCKED + ", " + EMAIL + ", " + FIRSTNAME + ", " + LASTNAME + ", " + MIDDLENAME +
                ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRole().name());
        statement.setString(5, user.getLocked().toString());
        statement.setString(6, user.getEmail());
        statement.setString(7, user.getFirstName());
        statement.setString(8, user.getLastName());
        statement.setString(9, user.getMiddleName());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void lockUserById(@NotNull String id) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET " + LOCKED + " = ? WHERE " + ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, Boolean.TRUE.toString());
        statement.setString(2, id);
        System.out.println(id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void unlockUserById(@NotNull String id) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET " + LOCKED + " = ? WHERE " + ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, Boolean.FALSE.toString());
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void updateUser(@NotNull User user) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET " +
                FIRSTNAME + " = ?, " + LASTNAME + " = ?, " + MIDDLENAME + " = ?, " + PASSWORD_HASH +
                " = ? WHERE " + ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getFirstName());
        statement.setString(2, user.getLastName());
        statement.setString(3, user.getMiddleName());
        statement.setString(4, user.getPasswordHash());
        statement.setString(5, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeById(@Nullable final String id) {
        @Nullable final User user = findByLogin(id);
        if (user == null) return null;
        @NotNull final String sql = "DELETE FROM " + getTableName() +
                " WHERE " + ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getId());
        statement.executeUpdate();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        @NotNull final String sql = "DELETE FROM " + getTableName() +
                " WHERE " + LOGIN + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getLogin());
        statement.executeUpdate();
        statement.close();
        return user;
    }

}

