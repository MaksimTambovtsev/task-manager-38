package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.repository.UserRepository;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    public UserService(@NotNull IConnectionService connection,
                       @Nullable final IUserRepository userRepository,
                       @NotNull IPropertyService propertyService,
                       @Nullable IProjectRepository projectRepository,
                       @Nullable ITaskRepository taskRepository) {
        super(userRepository, connection);
        this.propertyService = propertyService;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return null;
            return user;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = repository.findByEmail(email);
            if (user == null) return null;
            return user;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            repository.removeByLogin(login);
            connectionDB.commit();
        } catch (@NotNull final Exception e){connectionDB.rollback();}
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String login, @Nullable final String password) {
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = new User();
            user.setRole(Role.USUAL);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.create(user);
            connectionDB.commit();
        } catch (@NotNull final Exception e){connectionDB.rollback();}
        finally {
            connectionDB.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String email) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            repository.create(user);
            connectionDB.commit();
        } catch (@NotNull final Exception e){connectionDB.rollback();}
        finally {
            connectionDB.close();
        }
    }

    @SneakyThrows
    public void create(final String login, final String password, final Role role) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = new User();
            user.setRole(role);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.create(user);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = repository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            repository.updateUser(user);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User updateUser(@Nullable final String userId, @Nullable final String firstName,
                           @Nullable final String lastName, @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = repository.findById(userId);
            if (user == null) return null;
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.updateUser(user);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = findByLogin(login);
            repository.lockUserById(user.getId());
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final Connection connectionDB = connection.getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connectionDB);
        try {
            @Nullable final User user = findByLogin(login);
            repository.unlockUserById(user.getId());
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Override
    public @NotNull IRepository<User> getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }
}
