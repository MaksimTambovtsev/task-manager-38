package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

public interface IRepository<M extends AbstractEntity> {

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    M removeById(@Nullable String id);

}
