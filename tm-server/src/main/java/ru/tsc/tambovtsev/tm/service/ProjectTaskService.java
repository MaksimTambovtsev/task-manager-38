package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.IProjectTaskService;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.repository.ProjectRepository;
import ru.tsc.tambovtsev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @Nullable
    private final IProjectRepository projectRepository;

    @Nullable
    private final ITaskRepository taskRepository;

    @Nullable
    private final IConnectionService connectionService;

    public ProjectTaskService(@Nullable final IProjectRepository projectRepository,
                              @Nullable final ITaskRepository taskRepository,
                              @Nullable IConnectionService connectionService) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId,
                                  @Nullable final String taskId) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connectionService.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connectionDB);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            task.setProjectId(projectId);
            repository.updateById(task);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@Nullable final String userId,
                                      @Nullable final String taskId) throws AbstractException {
        Optional.ofNullable(taskId).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connectionService.getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connectionDB);
        try {
            @Nullable final Task task = repository.findById(userId, taskId);
            if (task == null) return;
            task.setProjectId(null);
            repository.updateById(task);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws AbstractException {
        Optional.ofNullable(projectId).orElseThrow(IdEmptyException::new);
        @NotNull final Connection connectionDB = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(connectionDB);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(connectionDB);
        try {
            taskRepository.removeByProjectId(userId, projectId);
            projectRepository.removeById(projectId);
            connectionDB.commit();
        } catch (@NotNull final Exception e){
            connectionDB.rollback();
            throw e;
        }
        finally {
            connectionDB.close();
        }
    }

}
