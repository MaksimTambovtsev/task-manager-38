package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void create(@Nullable User user);

    void lockUserById(@NotNull String id);

    void unlockUserById(@NotNull String id);

    void updateUser(@NotNull User user);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

}
