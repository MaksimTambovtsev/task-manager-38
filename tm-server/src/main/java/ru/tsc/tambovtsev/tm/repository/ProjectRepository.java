package ru.tsc.tambovtsev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.repository.IProjectRepository;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "TM_PROJECT";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @SneakyThrows
    public void create(@NotNull Project project) {
        @NotNull final String sql = "INSERT " + getTableName() +
                "(" +
                ID + ", " + NAME + ", " + DESCRIPTION + ", " + STATUS + ", " + USER_ID +
                ") VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getStatus().name());
        statement.setString(5, project.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull Project project) {
        @NotNull final String sql = "UPDATE " + getTableName() +
                " SET " +
                NAME + " = ?, " + DESCRIPTION + " = ?, " + STATUS + " = ?, " + USER_ID +
                " = ? WHERE " + ID + " = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setString(3, project.getStatus().name());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString(ID));
        project.setName(row.getString(NAME));
        project.setDescription(row.getString(DESCRIPTION));
        project.setUserId(row.getString(USER_ID));
        project.setStatus(Status.valueOf(row.getString(STATUS)));
        return project;
    }

}
