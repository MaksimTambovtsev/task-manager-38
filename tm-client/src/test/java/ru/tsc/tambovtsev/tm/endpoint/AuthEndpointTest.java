package ru.tsc.tambovtsev.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.dto.request.UserLoginRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserProfileRequest;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserProfileResponse;
import ru.tsc.tambovtsev.tm.marker.SoapCategory;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void testLogin() {
        final UserLoginResponse userLoginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertNotNull(userLoginResponse);
        final String token = userLoginResponse.getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void testProfile() {
        final UserLoginResponse userLoginResponse = authEndpoint.login(new UserLoginRequest("admin", "admin"));
        final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userProfileResponse.getUser().getLogin());
    }

}
