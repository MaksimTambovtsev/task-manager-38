package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractEntity> extends IRepository<M> {

    @Nullable
    M add(@Nullable M model);

    @Nullable
    List<M> findAll();

    @Nullable
    M remove(@Nullable M model);

    void clear();

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    List<M> findAll(@Nullable Sort sort);

    void removeAll(@Nullable Collection<M> collection);

    int getSize();

    boolean existsById(@Nullable String id);

}

