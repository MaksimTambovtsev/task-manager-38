package ru.tsc.tambovtsev.tm.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DomainSaveXMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save-xml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in xml fasterxml";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
