package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Task;

@NoArgsConstructor
public final class TaskBindProjectResponse extends AbstractTaskResponse {

    public TaskBindProjectResponse(@Nullable Task task) {
        super(task);
    }

}
